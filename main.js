window.addEventListener("load", inicio);

function inicio() {
    $('.nav-item').click(function(e) {
        $('.current_item').removeClass('current_item');
        $(this).addClass('current_item');
    });
    $(location).attr('#caja1');
    submitButton.addEventListener("click", createMatrix);

    $(document).on("scroll", onScroll);
}

function createMatrix() {
    error.innerHTML = '';
    if (validateDimension(dimension.value)) {
        form.className = "d-none";
        matrixContainer.className = '';
        for (let i = 1; i <= dimension.value; i++) {
            let row = document.createElement("tr");
            for (let j = 1; j <= dimension.value; j++) {
                let input = document.createElement("input");
                input.type = 'number'
                input.id = 'a' + i + j;
                input.className = "input-matrix text-center";
                row.appendChild(input)
            }
            container.appendChild(row)
        }
        calculate.addEventListener("click", calculateMatrix);
        backButton.addEventListener("click", back);
    } else {
        invalid('Por favor, ingrese un valor entre 1 y 16');
    }
}

function calculateMatrix() {
    let matrix = [];
    let valid_matrix = true;
    for (let i = 1; i <= dimension.value; i++) {
        let row = []
        for (let j = 1; j <= dimension.value; j++) {
            let id = 'a' + i + j;
            let input = eval(document.getElementById(id).value);
            if (document.getElementById(id).value == "") {
                valid_matrix = false;
                break;
            }
            row.push(input);
        }
        matrix.push(row);
    }
    var determinante = calculateDeterminante(matrix);
    let valid = true;
    if (isNaN(determinante) || !valid_matrix) {
        determinante = 'inválido'
        valid = false;
    }    
    showDeterminante('El determinante es: ' + determinante, valid);
}

function calculateDeterminante(matrix) {
    var row = matrix[0] || matrix;
    var determinante = 0;
    if (matrix.length == 1) {
        return matrix[0][0]
    } else if (matrix.length == 2) {
        return determinante2x2(matrix);
    } else {
        for (let i = 1; i <= row.length; i++) {
            let element = row[i - 1] * Math.pow(-1, i + 1);
            var adjunto = calculateAdjunto(matrix, 1, i);
            if (adjunto.length == 2) {
                determinante += element * determinante2x2(adjunto);
            } else {
                determinante += element * calculateDeterminante(adjunto);
            }
        }
    }
    return determinante;
}

function calculateAdjunto(matrix, position_one, position_two) {
    let adjunto = copyMatrix(matrix);
    adjunto.splice(position_one - 1, 1)
    for(var i = 0 ; i < adjunto.length ; i++) {
        adjunto[i].splice(position_two - 1, 1);
    }
    return adjunto;
}

function copyMatrix(matrix) {
    let new_array = [[]];
    for (let index = 0; index < matrix.length; index++) {
        new_array[index] = matrix[index].slice(0);
    }
    return new_array;
}

function determinante2x2(matrix) {
    value_one = matrix[0][0] * matrix[1][1];
    value_two = matrix[0][1] * matrix[1][0];
    return value_one - value_two;
}

function validateDimension(dimension) {
    if (dimension > 16 || dimension < 1) {
        return false;
    }
    return true;
}

function invalid(message) {
    error.className = 'font-white'
    error.innerHTML = message;
}

function showDeterminante(message, valid) {
    if (valid) {
        Swal.fire(
            'Bien hecho!',
            message,
            'success'
        )
    } else {
        Swal.fire(
            'Upss!',
            message,
            'error'
        )
    }
}

function back() {
    $('tr').remove();
    $('.input-matrix').remove();
    matrixContainer.className = 'd-none';
    form.className = 'form-inline row md-form';
    dimension.value = '';
}

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    $('#menu-center a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() + 75 > scrollPos) {
            $('#menu-center ul li').removeClass("current_item");
            currLink.parents('li').addClass("current_item");
        }
        else{
            currLink.parents('li').removeClass("current_item");
        }
    });
}
